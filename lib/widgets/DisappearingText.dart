/// @author erick
/// @since 2019-01-09
import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:flutter/widgets.dart";

class DisappearingText extends StatefulWidget {
  final String text;
  final AnimationController animationController;

  DisappearingText(this.animationController, this.text);

  @override
  State createState() => _OpacityState(animationController, text);
}

class _OpacityState extends State<DisappearingText>
    with SingleTickerProviderStateMixin {
  final String text;
  final AnimationController animationController;

  _OpacityState(this.animationController, this.text);

  @override
  Widget build(BuildContext context) {
    return AnimatedText(animationController, text);
  }
}

class AnimatedText extends AnimatedWidget {
  final String text;
  final AnimationController animationController;

  final double width = 200;
  final double height = 80;

  AnimatedText(this.animationController, this.text)
      : super(listenable: animationController);

  @override
  Widget build(BuildContext context) {
    return animationController.value < 1
          ? Text(text,
              style: TextStyle(
                fontSize: 26,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(255, 255, 255, 1 - animationController.value),
              ))
          : Container();
  }
}
