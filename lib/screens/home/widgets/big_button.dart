/// @author erick
/// @since 2018-12-07
import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import 'package:flutter_frp_boilerplate/widgets/DisappearingText.dart';

class BigButton extends StatefulWidget {
  final IconData iconData;
  final String caption;
  final Function onClick;

  BigButton(this.iconData, this.caption, this.onClick);

  @override
  State createState() => _ButtonWidthState(iconData, caption, onClick);
}

class _ButtonWidthState extends State<BigButton>
    with SingleTickerProviderStateMixin {
  final IconData iconData;
  final String caption;
  final Function onClick;

  AnimationController _controller;

  _ButtonWidthState(this.iconData, this.caption, this.onClick);

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedSizedBox(iconData, caption, onClick, _controller);
  }
}

class AnimatedSizedBox extends AnimatedWidget {
  final IconData iconData;
  final String caption;
  final Function onClick;
  final AnimationController _controller;

  final double width = 200;
  final double height = 80;

  AnimatedSizedBox(this.iconData, this.caption, this.onClick, this._controller)
      : super(listenable: _controller);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width + _controller.value * 200,
      child: _controller.value < 1
          ? FlatButton(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(
                    iconData,
                    color: CupertinoColors.white,
                    size: 32,
                  ),
                  DisappearingText(_controller, caption),
                ],
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              onPressed: () {
                _controller.forward();
                onClick();
              },
              color: Color.fromRGBO(0, 0, 0, 1 - _controller.value),
            )
          : Container(),
    );
  }
}
