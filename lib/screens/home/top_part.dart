/// @author erick
/// @since 2018-12-07
import "package:flutter/cupertino.dart";

class TopPart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: CupertinoColors.white),
        child: Center(child: Text("Hello")),
      ),
    );
  }
}
