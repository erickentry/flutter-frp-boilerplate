import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_frp_boilerplate/screens/home/bottom_part.dart';
import 'package:flutter_frp_boilerplate/screens/home/top_part.dart';
import 'package:flutter_frp_boilerplate/screens/home/widgets/big_button.dart';

/// @author erick
/// @since 2018-12-07

class Home extends StatelessWidget {
  const Home();

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: "Test",
      home: CupertinoPageScaffold(
          child: Stack(children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            TopPart(),
            BottomPart(),
          ],
        ),
        Center(
          child: BigButton(Icons.add, "Add Item", () {
            debugPrint(DateTime.now().millisecondsSinceEpoch.toString());
          }),
        )
      ])),
    );
  }
}
