/// @author erick
/// @since 2018-12-07
import "package:flutter/cupertino.dart";

class BottomPart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(color: Color(0x88EEEEEE)),
        child: Center(child: Text("World")),
      ),
    );
  }
}
