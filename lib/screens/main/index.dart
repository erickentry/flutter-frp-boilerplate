import 'package:flutter/material.dart';
import 'package:flutter_frp_boilerplate/bloc_provider.dart';
import 'package:flutter_frp_boilerplate/screens/main/bloc.dart';
import 'package:flutter_frp_boilerplate/screens/about/index.dart';

class Main extends StatelessWidget {
  const Main();

  @override
  Widget build(BuildContext context) {
    MainBloc bloc = BlocProvider.of<MainBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("State Reload Test"),
      ),
      body: Center(
        child: StreamBuilder(
          initialData: 0,
          stream: bloc.getSelectedIndex(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return RaisedButton(
                child: Text(snapshot.data.toString()),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => const About()));
                },
              );
            }
          },
        ),
      ),
      bottomNavigationBar: StreamBuilder(
        initialData: 0,
        stream: bloc.getSelectedIndex(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return BottomNavigationBar(
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(Icons.home), title: Text("Home")),
                BottomNavigationBarItem(
                    icon: Icon(Icons.business), title: Text("Business")),
                BottomNavigationBarItem(
                    icon: Icon(Icons.school), title: Text("School")),
              ],
              currentIndex: snapshot.data,
              fixedColor: Colors.deepPurple,
              onTap: (int index) {
                bloc.setSelectedIndex(index);
              },
            );
          }
        },
      ),
    );
  }
}
