import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_frp_boilerplate/bloc_provider.dart';

class MainBloc implements BlocBase {

  final _selectedIndex = PublishSubject<int>();

  MainBloc() {
    _selectedIndex.sink.add(0);
  }

  void setSelectedIndex(int index) {
    _selectedIndex.sink.add(index);
  }

  Stream getSelectedIndex() {
    return _selectedIndex.stream;
  }

  void dispose() {
    _selectedIndex.close();
  }
}