import 'package:flutter_frp_boilerplate/screens/main/index.dart';
import 'bloc_provider.dart';
import 'package:flutter_frp_boilerplate/screens/main/bloc.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(BlocProvider(
    bloc: MainBloc(),
    child: MaterialApp(
      home: const Main(),
    ),
  ));
}
