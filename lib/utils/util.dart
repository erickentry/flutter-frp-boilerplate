/// @author erick
/// @since 2018-12-06

/// https://github.com/dart-lang/site-www/issues/736
T safeCast<T>(dynamic obj,
    {bool ignoreNull = true, bool assertResult = false}) {
  if (obj == null) {
    if (!ignoreNull) {
      final error = "safeCast passed in obj is null";
      print(error);
      assert(false, error);
  }
  return null;
  }

  final T result = (obj is T) ? obj : null;
  if (result == null && obj != null) {
  // obj will never be null, but this looks more clear
  final error = "safeCast obj:$obj is not of type:${T.runtimeType}";
  print(error);
  if (assertResult) assert(false, error);
  }
  return result;
}